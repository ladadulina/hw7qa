import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.checkerframework.checker.units.qual.A;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Hw7Test extends BaseTest {

    @Test
    public void createAccountTest() {
        openHomePage();
        HomePage homePage = new HomePage(driver);
        homePage.clickLogInButton();

        Login login = new Login(driver);
        String emailName = RandomStringUtils.randomAlphabetic(8);
        String email = emailName + "@gmail.com";

        login.fillEmail(email);
        login.submitButton();

        login.clickIdGender()
                .fillFirstName("Lada")
                .fillLastName("Protcheva")
                .fillPassword("harius12")
                .fillDayOfBirth(21)
                .fillMonthOfBirth(9)
                .fillYearOfBirth("2003")
                .fillAddress("Lafayette street")
                .fillCity("Detroit")
                .fillPostcode("00000")
                .fillState("22")
                .fillPhone("752891364")
                .fillStreet("Lafayette");

        driver.findElement(By.id("submitAccount")).click();
        String userText = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).getText();

        Assert.assertTrue(userText.contains("Lada Protcheva"));

    }

    @Test
    public void isBought() {
        ShoppingProcess shoppingProcess = new ShoppingProcess(driver);

        shoppingProcess.selectCategory();
        shoppingProcess.selectItem();

        shoppingProcess.changeQuantity("2")
                .selectSize("M")
                .selectColor("color_24");

        shoppingProcess.addToCartButton();
        shoppingProcess.proceedToCheckoutButton();
        shoppingProcess.proceedToCheckoutSecondButton();
        shoppingProcess.proceedToCheckoutThirdButton();
        shoppingProcess.proceedToCheckoutFourthButton();
        shoppingProcess.agreementButton();
        shoppingProcess.proceedToCheckoutFifthButton();
        shoppingProcess.confirmOrderButton();

        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/p[1]")).isDisplayed());
        driver.quit();
    }
}
