import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

public class CreateAccount {
    public WebDriver driver;

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    public boolean createAccount() {
        String emailName = RandomStringUtils.randomAlphabetic(8);
        String email = emailName + "@gmail.com";
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/");
        (new WebDriverWait(driver, 40)).
                until(ExpectedConditions.visibilityOfElementLocated(By.className("login")));
        driver.findElement(By.className("login")).click();
        driver.findElement(By.id("email_create")).sendKeys(email);

        driver.findElement(By.id("SubmitCreate")).click();//create an account button
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.name("id_gender")).click();
        driver.findElement(By.id("customer_firstname")).sendKeys("Lada");
        driver.findElement(By.id("customer_lastname")).sendKeys("Protcheva");

        driver.findElement(By.id("passwd")).sendKeys("harius12");

        Select dayOfBirth = new Select(driver.findElement(By.id("days")));
        dayOfBirth.selectByIndex(21);

        Select monthOfBirth = new Select(driver.findElement(By.id("months")));
        monthOfBirth.selectByIndex(9);

        Select yearOfBirth = new Select(driver.findElement(By.id("years")));
        yearOfBirth.selectByValue("2003");

        driver.findElement(By.id("address1")).sendKeys("Lafayette street");

        driver.findElement(By.id("city")).sendKeys("Detroit");

        driver.findElement(By.id("postcode")).sendKeys("00000");

        WebElement state = driver.findElement(By.name("id_state"));
        Select selectedState = new Select(state);
        selectedState.selectByValue("22");

        driver.findElement(By.id("phone_mobile")).sendKeys("752891364");

        driver.findElement(By.id("alias")).sendKeys("Lafayette");

        driver.findElement(By.id("submitAccount")).click();


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String userText = driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).getText();

       // Assert.assertTrue(userText.contains("Protcheva"));

        if (userText.contains("Protcheva")) {
          return true;
        } else {
           return false;
        }

    }
}