import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShoppingProcess extends BasePage{

    public ShoppingProcess(WebDriver driver) {
        super.driver = driver;
    }

    public void selectCategory() {
        driver.findElement(By.className("sf-with-ul")).click();
    }

    public void selectItem(){
        WebElement image = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div[2]/ul/li[4]/div/div[1]/div/a[1]/img"));
        WebElement moreButton = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div[2]/ul/li[4]/div/div[2]/div[2]/a[2]/span"));
        Actions actions = new Actions(driver);
        actions.moveToElement(image).moveToElement(moreButton).click().perform();
    }

    public ShoppingProcess changeQuantity(String quantity){

        driver.findElement(By.id("quantity_wanted")).clear();
        driver.findElement(By.id("quantity_wanted")).sendKeys(quantity);
        return this;
    }

    public ShoppingProcess selectSize(String size1){

        WebElement size = driver.findElement(By.xpath("//*[@id='group_1']"));
        Select oSelect = new Select(size);
        oSelect.selectByVisibleText(size1);
        return this;
    }

    public ShoppingProcess selectColor(String color){

        driver.findElement(By.id(color)).click();
        return this;
    }

    public void addToCartButton(){

        driver.findElement(By.xpath("//p[@id='add_to_cart']//span[.='Add to cart']")).click();
    }

    public void proceedToCheckoutButton(){

        (new WebDriverWait(driver, 40)).
                until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html//div[@id='layer_cart']//a[@title='Proceed to checkout']/span")));
        driver.findElement(By.xpath("/html//div[@id='layer_cart']//a[@title='Proceed to checkout']/span")).click();
    }

    public void proceedToCheckoutSecondButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/p[2]/a[1]/span")).click();
    }

    public void proceedToCheckoutThirdButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/p/button/span")).click();
    }

    public void proceedToCheckoutFourthButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div/p[2]/div/span/input")).click();
    }

    public void agreementButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/p/button/span")).click();
    }

    public void proceedToCheckoutFifthButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[3]/div[2]/div/p/a")).click();
    }

    public void confirmOrderButton(){
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/p/button/span")).click();
    }
}
