import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {


    public HomePage(WebDriver driver) {
        super.driver = driver;
    }

    public void clickLogInButton() {
        (new WebDriverWait(driver, 40)).
                until(ExpectedConditions.visibilityOfElementLocated(By.className("login"))).click();

        //driver.findElement(By.className("login")).click();
    }
}
