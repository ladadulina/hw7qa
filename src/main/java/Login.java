import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login extends BasePage{

    public Login(WebDriver driver) {
       super.driver = driver;
    }

    public void fillEmail(String email){
        driver.findElement(By.id("email_create")).sendKeys(email);

    }

    public void submitButton(){
        driver.findElement(By.id("SubmitCreate")).click();

    }

    public Login clickIdGender(){
        (new WebDriverWait(driver, 40)).
                until(ExpectedConditions.visibilityOfElementLocated(By.name("id_gender")));
        driver.findElement(By.name("id_gender")).click();
        return this;
    }

    public Login fillFirstName(String firstName){
        driver.findElement(By.id("customer_firstname")).sendKeys(firstName);
   return this;
    }

    public Login fillLastName(String lasName) {
        driver.findElement(By.id("customer_lastname")).sendKeys(lasName);
   return this;
    }

    public Login fillPassword(String passwd) {
        driver.findElement(By.id("passwd")).sendKeys(passwd);
    return this;
    }

    public Login fillDayOfBirth(int day){
        Select dayOfBirth = new Select(driver.findElement(By.id("days")));
        dayOfBirth.selectByIndex(day);
        return this;
    }
    public Login fillMonthOfBirth(int month){
        Select monthOfBirth = new Select(driver.findElement(By.id("months")));
        monthOfBirth.selectByIndex(month);
        return this;
    }


    public Login fillYearOfBirth(String year){
        Select yearOfBirth = new Select(driver.findElement(By.id("years")));
        yearOfBirth.selectByValue(year);
        return this;
    }

    public Login fillAddress(String address) {
        driver.findElement(By.id("address1")).sendKeys(address);
   return this;
    }

    public Login fillCity(String city) {
        driver.findElement(By.id("city")).sendKeys(city);
    return this;
    }

    public Login fillPostcode(String postcode) {
        driver.findElement(By.id("postcode")).sendKeys(postcode);
   return this;
    }

    public Login fillState(String state1) {
        WebElement state = driver.findElement(By.name("id_state"));
        Select selectedState = new Select(state);
        selectedState.selectByValue(state1);
        return this;
    }

    public Login fillPhone(String phone) {
        driver.findElement(By.id("phone_mobile")).sendKeys(phone);
  return this;
    }

    public Login fillStreet(String street) {
        driver.findElement(By.id("alias")).sendKeys(street);
   return this;
    }
}
